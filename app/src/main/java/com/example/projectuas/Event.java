package com.example.projectuas;

public class Event {
    public String id;
    public String category;
    public String name;
    public Integer price;
    public String description;
    public String date;
    public Integer image;

    public Event(String id, String category, String name, Integer price, String description, String date, Integer image) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.price = price;
        this.description = description;
        this.date = date;
        this.image = image;
    }

    // Getter and Setter
    public String get_name() {
        return name;
    }

    public String get_category() {
        return category;
    }

    public int get_price() {
        return price;
    }

    public String get_date() {
        return date;
    }

    public String get_description() { return description; }

    public int get_image() {
        return image;
    }

}
