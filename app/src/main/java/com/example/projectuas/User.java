package com.example.projectuas;

public class User {
    public String id;
    public String fullname;
    public String email;
    public String password;

    public User(String id, String fullname, String email, String password) {
        this.id = id;
        this.fullname = fullname;
        this.email = email;
        this.password = password;
    }

}
