package com.example.projectuas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class EventsDetailActivity extends AppCompatActivity {
    private ArrayList<Event> eventArrayList;
    private Integer eventId;

    private ImageView imgEvent;
    private TextView textEventName, textEventDate, textEventPrice, textEventDescription;

    Button btnFollow;

    SqliteHelper sqliteHelper;

    Event model;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_detail);
        sharedPreferences = getSharedPreferences("user_details", MODE_PRIVATE);
        sqliteHelper = new SqliteHelper(this);
        eventArrayList = new ArrayList<>();
        eventArrayList.add(new Event("1", "TEKNOLOGI", "Pelatihan Amazon Web Service", 50000, "Pelatihan yang diselenggarakan oleh STMIK AMIK ini diharapkan akan meningkatkan skill para mahasiswa", "05-Juli-2021", R.drawable.teknologi));
        eventArrayList.add(new Event("2", "EKONOMI", "Mencari Peluang Ditengah Pandemi", 500000, "Bosan terus berdiam diri dirumah ? mari gabung untuk menemukan peluang di masa sulit seperti saat ini", "15-Juli-2021", R.drawable.ekonomi));
        eventArrayList.add(new Event("3", "SEJARAH", "Sejarah Terbentuknya Sunda Empire", 5000, "Belajar dari sejarah dapat membuat kita belajar akan kesalahan yang telah dilakukan orang-orang sebelum kita, ayo gabung sekarang", "25-Juli-2021", R.drawable.sejarah));

        eventId = getIntent().getIntExtra("event_id", 1);
        model = eventArrayList.get(eventId);
        imgEvent = (ImageView) findViewById(R.id.imgEvent);
        textEventName = (TextView) findViewById(R.id.textEventName);
        textEventDate = (TextView) findViewById(R.id.textEventDate);
        textEventPrice = (TextView) findViewById(R.id.textEventPrice);
        textEventDescription = (TextView) findViewById(R.id.textEventDescription);
        btnFollow = (Button) findViewById(R.id.btnFollow);

        imgEvent.setImageResource(model.get_image());
        textEventName.setText(model.get_name());
        textEventDate.setText(model.get_date());
        textEventPrice.setText("Rp. " + String.valueOf(model.get_price()));
        textEventDescription.setText(model.get_description());

    }

    public void addToCart(View view) {
        Cart cart = new Cart(null, Integer.parseInt(sharedPreferences.getString("user_id", null)) , eventId+1);
        sqliteHelper.addToCart(cart);
        Snackbar.make(btnFollow, "Event added to cart successfully!", Snackbar.LENGTH_LONG).show();
        Intent intent=new Intent(EventsDetailActivity.this,HomeActivity.class);
        startActivity(intent);
        finish();
    }
}