package com.example.projectuas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity {

    EditText editTextEmail;
    EditText editTextPassword;

    Button btnLogin;

    SqliteHelper sqliteHelper;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sqliteHelper = new SqliteHelper(this);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        sharedPreferences = getSharedPreferences("user_details", MODE_PRIVATE);
        sharedPreferences.contains("user_id");
        sharedPreferences.contains("fullname");
        sharedPreferences.contains("email");
    }

    public void login(View view) {
        //if (validate()) {
            String Email = editTextEmail.getText().toString();
            String Password = editTextPassword.getText().toString();

            User currentUser = sqliteHelper.Authenticate(new User(null, null, Email, Password));

            //Check Authentication is successful or not
            if (currentUser != null) {
                Snackbar.make(btnLogin, "Successfully Logged in!", Snackbar.LENGTH_LONG).show();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("user_id", currentUser.id);
                editor.putString("fullname", currentUser.fullname);
                editor.putString("email", currentUser.email);
                editor.apply();

                //User Logged in Successfully Launch You home screen activity
                Intent intent=new Intent(LoginActivity.this,HomeActivity.class);
                startActivity(intent);
                finish();

            } else {

                //User Logged in Failed
                Snackbar.make(btnLogin, "Failed to log in , please try again", Snackbar.LENGTH_LONG).show();

            }

        //}
    }

    public void showRegister(View view) {
        Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    //This method is used to validate input given by user
    public boolean validate() {
        boolean valid = false;

        //Get values from EditText fields
        String Email = editTextEmail.getText().toString();
        String Password = editTextPassword.getText().toString();

        //Handling validation for Email field
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(Email).matches()) {
            valid = false;
        } else {
            valid = true;
        }

        //Handling validation for Password field
        if (Password.isEmpty()) {
            valid = false;
        } else {
            if (Password.length() > 5) {
                valid = true;
            } else {
                valid = false;
            }
        }

        return valid;
    }
}