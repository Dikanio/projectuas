package com.example.projectuas;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;


public class SqliteHelper extends SQLiteOpenHelper {
    private ArrayList<Event> eventArrayList;
    private ArrayList<Event> cartArrayList;

    //DATABASE NAME
    public static final String DATABASE_NAME = "project_uas";

    //DATABASE VERSION
    public static final int DATABASE_VERSION = 1;

    //TABLE NAME
    public static final String TABLE_USERS = "users";
    public static final String TABLE_EVENTS = "events";
    public static final String TABLE_CARTS = "carts";

    //TABLE USERS COLUMNS
    //ID COLUMN @primaryKey
    public static final String KEY_ID = "id";

    //COLUMN user name
    public static final String KEY_FULL_NAME = "fullname";

    //COLUMN email
    public static final String KEY_EMAIL = "email";

    //COLUMN password
    public static final String KEY_PASSWORD = "password";

    //SQL for creating users table
    public static final String SQL_TABLE_USERS = " CREATE TABLE " + TABLE_USERS
            + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY, "
            + KEY_FULL_NAME + " TEXT, "
            + KEY_EMAIL + " TEXT, "
            + KEY_PASSWORD + " TEXT"
            + " ) ";

    //TABLE EVENTS COLUMNS
    public static final String KEY_EVENTS_ID = "id";
    public static final String KEY_EVENTS_CATEGORY = "category";
    public static final String KEY_EVENTS_NAME = "name";
    public static final String KEY_EVENTS_PRICE = "price";
    public static final String KEY_EVENTS_DESCRIPTION = "description";
    public static final String KEY_EVENTS_DATE = "date";

    //SQL for creating users table
    public static final String SQL_TABLE_EVENTS = " CREATE TABLE " + TABLE_EVENTS
            + " ( "
            + KEY_EVENTS_ID + " INTEGER PRIMARY KEY, "
            + KEY_EVENTS_CATEGORY + " TEXT, "
            + KEY_EVENTS_NAME + " TEXT, "
            + KEY_EVENTS_PRICE + " INTEGER, "
            + KEY_EVENTS_DESCRIPTION + " TEXT, "
            + KEY_EVENTS_DATE + " TEXT"
            + " ) ";

    //TABLE Cart COLUMNS
    public static final String KEY_CART_ID = "id";
    public static final String KEY_CART_USER_ID = "user_id";
    public static final String KEY_CART_EVENT_ID = "event_id";

    //SQL for creating users table
    public static final String SQL_TABLE_CARTS = " CREATE TABLE " + TABLE_EVENTS
            + " ( "
            + KEY_CART_ID + " INTEGER PRIMARY KEY, "
            + KEY_CART_USER_ID + " INTEGER, "
            + KEY_CART_EVENT_ID + " INTEGER "
            + " ) ";

    public SqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Create Table when oncreate gets called
        sqLiteDatabase.execSQL(SQL_TABLE_USERS);
        sqLiteDatabase.execSQL(SQL_TABLE_CARTS);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //drop table to create new one if database version updated
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_USERS);
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_CARTS);
    }

    //using this method we can add users to user table
    public void addUser(User user) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        //create content values to insert
        ContentValues values = new ContentValues();

        //Put fullname in  @values
        values.put(KEY_FULL_NAME, user.fullname);

        //Put email in  @values
        values.put(KEY_EMAIL, user.email);

        //Put password in  @values
        values.put(KEY_PASSWORD, user.password);

        // insert row
        long todo_id = db.insert(TABLE_USERS, null, values);
    }

    public void addToCart(Cart cart) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        //create content values to insert
        ContentValues values = new ContentValues();

        values.put(KEY_CART_USER_ID, cart.user_id);

        values.put(KEY_CART_EVENT_ID, cart.event_id);

        // insert row
        long todo_id = db.insert(TABLE_CARTS, null, values);
    }

    public ArrayList<Event> getItemCart(Integer user_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        cartArrayList = new ArrayList<>();
        Cursor cursor = db.query(TABLE_CARTS,// Selecting Table
                new String[]{KEY_CART_ID, KEY_CART_USER_ID, KEY_CART_EVENT_ID},//Selecting columns want to query
                KEY_CART_USER_ID + "=?",
                new String[]{String.valueOf(user_id)},//Where clause
                null, null, null);
        if (cursor.moveToFirst()){
            do {
                // Passing values
                Integer eventId = Integer.parseInt(cursor.getString(2));
                // Do something Here with values
                eventArrayList = new ArrayList<>();
                eventArrayList.add(new Event("1", "TEKNOLOGI", "Pelatihan Amazon Web Service", 50000, "Pelatihan yang diselenggarakan oleh STMIK AMIK ini diharapkan akan meningkatkan skill para mahasiswa", "05-Juli-2021", R.drawable.teknologi));
                eventArrayList.add(new Event("2", "EKONOMI", "Mencari Peluang Ditengah Pandemi", 500000, "Bosan terus berdiam diri dirumah ? mari gabung untuk menemukan peluang di masa sulit seperti saat ini", "15-Juli-2021", R.drawable.ekonomi));
                eventArrayList.add(new Event("3", "SEJARAH", "Sejarah Terbentuknya Sunda Empire", 5000, "Belajar dari sejarah dapat membuat kita belajar akan kesalahan yang telah dilakukan orang-orang sebelum kita, ayo gabung sekarang", "25-Juli-2021", R.drawable.sejarah));
                Event model = eventArrayList.get(eventId-1);
                cartArrayList.add(model);
            } while(cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return cartArrayList;
    }

    public User Authenticate(User user) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USERS,// Selecting Table
                new String[]{KEY_ID, KEY_FULL_NAME, KEY_EMAIL, KEY_PASSWORD},//Selecting columns want to query
                KEY_EMAIL + "=?",
                new String[]{user.email},//Where clause
                null, null, null);

        if (cursor != null && cursor.moveToFirst()&& cursor.getCount()>0) {
            //if cursor has value then in user database there is user associated with this given email
            User user1 = new User(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));

            //Match both passwords check they are same or not
            if (user.password.equalsIgnoreCase(user1.password)) {
                return user1;
            }
        }

        //if user password does not matches or there is no record with that email then return @false
        return null;
    }

    public boolean isEmailExists(String email) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USERS,// Selecting Table
                new String[]{KEY_ID, KEY_FULL_NAME, KEY_EMAIL, KEY_PASSWORD},//Selecting columns want to query
                KEY_EMAIL + "=?",
                new String[]{email},//Where clause
                null, null, null);

        if (cursor != null && cursor.moveToFirst()&& cursor.getCount()>0) {
            //if cursor has value then in user database there is user associated with this given email so return true
            return true;
        }

        //if email does not exist return false
        return false;
    }
}