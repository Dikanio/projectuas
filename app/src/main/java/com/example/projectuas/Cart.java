package com.example.projectuas;

public class Cart {
    public String id;
    public Integer user_id;
    public Integer event_id;

    public Cart(String id, Integer user_id, Integer event_id) {
        this.id = id;
        this.user_id = user_id;
        this.event_id = event_id;
    }
}
