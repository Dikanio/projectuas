package com.example.projectuas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {
    RecyclerView rvEvent;

    private ArrayList<Event> eventArrayList;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        rvEvent = findViewById(R.id.rvEvent);
        // here we have created new array list and added data to it.
        eventArrayList = new ArrayList<>();
        eventArrayList.add(new Event("1", "TEKNOLOGI", "Pelatihan Amazon Web Service", 50000, "Pelatihan yang diselenggarakan oleh STMIK AMIK ini diharapkan akan meningkatkan skill para mahasiswa", "05-Juli-2021", R.drawable.teknologi));
        eventArrayList.add(new Event("2", "EKONOMI", "Mencari Peluang Ditengah Pandemi", 500000, "Bosan terus berdiam diri dirumah ? mari gabung untuk menemukan peluang di masa sulit seperti saat ini", "15-Juli-2021", R.drawable.ekonomi));
        eventArrayList.add(new Event("3", "SEJARAH", "Sejarah Terbentuknya Sunda Empire", 5000, "Belajar dari sejarah dapat membuat kita belajar akan kesalahan yang telah dilakukan orang-orang sebelum kita, ayo gabung sekarang", "25-Juli-2021", R.drawable.sejarah));

        // we are initializing our adapter class and passing our arraylist to it.
        EventAdapter eventAdapter = new EventAdapter(this, eventArrayList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rvEvent.setLayoutManager(linearLayoutManager);
        rvEvent.setAdapter(eventAdapter);
    }
}